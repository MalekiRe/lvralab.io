---
weight: 70
title: Envision
---

# Envision

<video width="640" height="360" controls>
  <source src="/video/envision_installation/envision_installation_hq.mp4" type="video/mp4">
  <source src="/video/envision_installation/envision_installation.webm" type="video/webm">
</video>

- [Envision GitLab repository](https://gitlab.com/gabmus/envision)

Envision is a graphical app that acts as an orchestrator to get a full [Monado](/docs/fossvr/monado/) or [WiVRn](/docs/fossvr/wivrn/) setup up and running with a few clicks.

Envision attempts to construct a working runtime with both a native OpenXR and an OpenVR API, provided by [OpenComposite](/docs/fossvr/opencomposite/), for client aplications to utilize. Please note the OpenVR implementation is incomplete and contains only what's necessary to run most games for compatibility. If you plan to implement software, utilize the OpenXR API, specification [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html).


{{< hint danger >}}
**Warning**

Envision is still to be considered alpha-quality and highly experimental.
{{< /hint >}}

You can download the latest Appimage snapshot from [GitLab Pipelines](https://gitlab.com/gabmus/envision/-/pipelines).

## Room Setup

If you're planning to use the SteamVR lighthouse driver (with Index or Vive HMDs, or other lighthouse tracked HMDs), you'll need to go through the SteamVR room setup. You can do that from SteamVR, directly from Envision or manually by running the appropriate scripts. More info in the [SteamVR page](/docs/steamvr).

## Experimental feature settings

The following resources can be entered into your envision profile repo and branch settings to enable early access to code before its fully upstream in monado itself. Simply edit your profile with these settings and rebuild to enable these feature sets.

### Full body lighthouse tracking

These patches enable fairly hacky full body support for steamvr_lh and survive drivers in monado. Both of these must be used in conjuction or your XR application will crash or hang.

For the Envision xr service settings:

Repo `https://gitlab.freedesktop.org/BabbleBones/monado/`

Branch `vive_tracker3`

For the Envision OpenComposite settings:

Repo `https://gitlab.com/BabbleBones/OpenOVR`

Branch `htcx-fbt`

### WMR Controller tracking

This enables positional tracking for WMR controllers in full 6dof

For the Envision xr service settings:

Repo `https://gitlab.freedesktop.org/thaytan/monado`

Branch `dev-constellation-controller-tracking`

### Basalt persistent space map

This allows for the saving and retreieval of the basalt SLAM tracking map. The upshot of which is a persistent space origin, floor, and room orientation on SLAM tracked headsets such as WMR. Consider pairing this with LOVR playspace to set room boundaries.

For the Envision Basalt settings:

Repo `https://github.com/CIFASIS/basalt-with-persistent-map`

Branch `xrtslam`

### Experimental Pimax support branch

Major WIP. Allows certain Pimax HMDs to function with Monado.

For the Envision xr service settings:

Repo `https://gitlab.freedesktop.org/Coreforge/monado/`

Branch `pimax`
